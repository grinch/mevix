<?php
set_time_limit(0);
define('BASEDIR', __DIR__);
//$start = microtime(true);
include('src/App.php');
include('config/config.php');

if (!isset($_POST['text'])) {
    echo json_encode(['status' => 'error', 'message'=> 'Wrong POST parameters']); die;
}

App::run($_POST['text'], $config);

$finish = microtime(true);
//echo("\r\n" . round($finish - $start, 4) . " c\r\n");