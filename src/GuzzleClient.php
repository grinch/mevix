<?php

use GuzzleHttp\Client;

class GuzzleClient extends Client
{
    private $url;
    private $token;
    public function __construct($url, $token)
    {
        parent::__construct();
        $this->url = $url;
        $this->token = $token;
    }

    public function getText($count, $offset = 0)
    {
        //count-1 похоже на баг в API. Отдает на 1 текст больше чем указан параметр count
        $url = (str_replace([':token', ':count', ':offset'], [$this->token, $count-1, $offset], $this->url));
        $array = $this->get($url)->json();
        return $array['response'];
    }
}