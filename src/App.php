<?php
defined('BASEDIR') OR exit('No direct script access allowed');

include(BASEDIR . '/vendor/autoload.php');

include(BASEDIR . '/src/Compare.php');
include(BASEDIR . '/src/GuzzleClient.php');

class App
{

    static function run($testText, $config)
    {
        $result['dublicate'] = 0;
        $i = 1;

        $compare = new Compare();
        $testText = $compare->canonizeText($testText);
        $testArray = $compare->hashWordArray($testText);

        $guzzle = new GuzzleClient($config['url'], $config['token']);

        for ($j = 1; $j <= $config['max']; $j = $j + $config['partition']) {

            foreach ($guzzle->getText($config['partition'],$j) as $textfromArray) {
                $similarity = $compare->compareTexts($testArray, $textfromArray);
                if ($similarity > $config['similarity']) {
                    $result['dublicate']++;
                }
                $result['similarity'][$i] = $similarity . '%';
                $i++;
            }
        }
        $result['status'] = 'ok';
        echo json_encode($result);
    }
}