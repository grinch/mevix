<?php

class Compare
{

    private $stopWords = [
        '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
        'без', 'более', 'бы', 'был', 'была', 'были', 'было', 'быть', 'вам', 'вас', 'ведь', 'весь', 'вдоль',
        'вместо', 'вне', 'вниз', 'внизу', 'во', 'вокруг', 'вот', 'все', 'всего', 'всех',
        'вы', 'где', 'да', 'давать', 'даже', 'для', 'до', 'достаточно', 'его', 'ее', 'её', 'если',
        'есть', 'ещё', 'же', 'за', 'здесь', 'из', 'из-за', 'или', 'им', 'иметь', 'их', 'как',
        'как-то', 'кто', 'кроме', 'кто', 'ли', 'либо', 'мне', 'может', 'мои', 'мой', 'мы', 'на',
        'над', 'надо', 'наш', 'не', 'него', 'неё', 'нет', 'ни', 'них', 'но', 'ну', 'об', 'однако', 'он', 'она', 'они',
        'оно', 'от', 'отчего', 'очень', 'по', 'под', 'после', 'потому',  'при', 'про', 'снова',
        'со', 'так', 'также', 'такие', 'такой', 'там', 'те', 'тем', 'то', 'того', 'тоже', 'той', 'только', 'том', 'тут',
        'ты', 'уже', 'хотя', 'чего', 'чего-то', 'чей', 'чем', 'что', 'чтобы', 'чьё', 'чья', 'эта', 'эти',
        'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'э', 'ь', 'ы', 'ю', 'я',
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
    private $stopSymbols = ['.', ',', '-', '—', '_', '=', '+', '/', '!', '”', ';', ':', '%', '?', '*', '(', ')','[', ']'];


    public function compareTexts($array, $text2)
    {
        $text4 = $this->canonizeText($text2);
        $array2 = $this->hashWordArray($text4);

        return $this->compareHashArrays($array,$array2);
    }

    public function canonizeText($text)
    {
        $text1 = trim(mb_strtolower(strip_tags($text), 'UTF-8'));
        $text2 = str_replace($this->stopSymbols, null, $text1);
        $text3 =  preg_replace('/\s{2,}/',' ',$text2);
        $arrayWords = explode(' ',$text3);

        foreach ($this->stopWords as $word) {
                $arrayWords = $this->arrayWordRemove($word, $arrayWords);
            }
	    return $arrayWords;
    }

    private function arrayWordRemove($word, array $array)
    {
        $count = 0;
        foreach ($array as $key => $value) {
            if ($word == $value) {
                array_splice($array, $key-$count, 1);
                $count++;
            }
        }
        return $array;
    }

    public function hashWordArray(array $wordArray)
    {
        $hashArray = [];
        foreach ($wordArray as $word) {
            $hashArray[] = crc32($word);
        }
        return $hashArray;
    }

    private function compareHashArrays(array $array1, array $array2)
    {
        $count = 0;
        foreach($array1 as $hash1){
            foreach ($array2 as $hash2) {
                if ($hash1 == $hash2) {
                    $count++;break;
                }

            }
        }
        //var_dump('текст1=' . count($array1), 'текст2=' . count($array2) , 'одинаковых слов:' .$count . "\r\n");
        return (round($count*2 / (count($array1) + count($array2)) * 100, 2));
    }
}
